SSWClient = function(consumer_key, consumer_secret, redirect_uri) {
  var _service_domain = Shopify.shop;
  var _scheme = '//';
  var _api_path = '/apps/ssw/api/';
  var _authorize_path = 'authorize';
  var _access_token_path = 'access_token';

  var api_url, authorize_url, access_token_url, token, self;

  api_url = _scheme + _service_domain + _api_path;
  authorize_url = api_url + _authorize_path;
  access_token_url = api_url + _access_token_path;

  self = this;
  this.getAuthorizationUri = function(hesid) {
    if (!redirect_uri) {
      throw new Exception('redirect_uri is not defined');
    }
    var data = {
      'response_type': 'code',
      'state': Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1),
      'client_id': consumer_key,
      'redirect_uri': redirect_uri
    };
    var authorizationUri = authorize_url + '?' + jQuery.param(data);

    return authorizationUri;
  };

  this.getAppToken = function(scopes, callback) {
    if (typeof scopes == 'function') {
      callback = scopes;
    }
    if (typeof scopes != 'string' || !scopes) {
      scopes = 'read_user write_user read_wishlist write_wishlist read_review write_review';
    }

    var payload = {
      'client_id': consumer_key,
      'client_secret': consumer_secret,
      'scope': scopes,
      'grant_type': 'client_credentials',
    };

    self.request('POST', _access_token_path, payload, function(response) {
      var result = false;
      if (response && typeof response.access_token != 'undefined') {
        result = true;
        self.setAccessToken(response.access_token);
      }
      if (typeof callback == 'function') {
        callback(result);
      }
    });
  };

  this.setAccessToken = function(access_token) {
    token = access_token;
  };

  this.call = function(method, path, params, callback) {
    var url = api_url + path;
    if (typeof path == 'undefined') {
      throw new Exception('You must specify a request url.');
    }
    if (typeof path !== 'string') {
      throw new Exception('String expected, got ' + typeof path);
    }

    params.access_token = token;
    if (method == 'PUT') {
      url += '?' + jQuery.param(params);
    }

    jQuery.ajax({
      'url': url,
      'method': method,
      'data': params,
      'headers': {'X-SSW-Access-Token': token}
    }).always(function(data, textStatus, jqXHR) {
      if (typeof callback == 'function') {
        callback(data, textStatus, jqXHR);
      }
    });

  };

  this.request = function(method, path, params, callback) {
    jQuery.ajax({
      'url': api_url + path,
      'method': method,
      'data': params
    }).always(function(data, textStatus, jqXHR) {
      if (typeof callback == 'function') {
        callback(data, textStatus, jqXHR);
      }
    });
  };

  this.getUsers = function(limit, page, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    self.call('GET', 'users', {'limit': limit, 'page': page}, callback);
  };

  this.getUser = function(user_id, callback) {
    self.call('GET', 'users/' + user_id, callback);
  };

  this.editUser = function(user_id, user_data, callback) {
    self.call('PUT', 'users/' + user_id, user_data, callback);
  };

  this.searchUser = function(email, callback) {
    self.call('GET', 'users/search', {'field': 'email', 'value': email}, callback);
  };

  this.getWishlist = function(user_id, board_id, limit, page, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof board_id !== 'number' || !board_id) {
      board_id = 0;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }
    var data = {'user_id': user_id, 'board_id': board_id, 'limit': limit, 'page': page};
    self.call('GET', 'wishlist', data, callback);
  };

  this.addToWishlist = function(user_id, product_id, board_id, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof board_id !== 'number' || !board_id) {
      board_id = 0;
    }
    var data = {'user_id': user_id, 'product_id': product_id, 'board_id': board_id};
    self.call('POST', 'wishlist', data, callback);
  };

  this.reviews = function(user_id, product_id, limit, page, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof user_id !== 'number' || !user_id) {
      user_id = 0;
    }
    if (typeof product_id !== 'number' || typeof product_id !== 'string' || !product_id) {
      product_id = null;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    var data = {'user_id': user_id, 'limit': limit, 'page': page};
    if (product_id !== null) {
      data.product_id = product_id;
    }
    self.call('GET', 'reviews', data, callback);
  };

  this.postReview = function(email, name, body, rate, product_id, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof email !== 'string' || !email) {
      email = '';
    }
    if (typeof name !== 'string' || !name) {
      name = '';
    }
    if (typeof body !== 'string' || !body) {
      body = '';
    }
    if (typeof rate !== 'number' || !rate) {
      rate = 0;
    }
    if (typeof product_id !== 'number' || typeof product_id !== 'string' || !product_id) {
      product_id = 0;
    }

    var data = {'email': email, 'name': name, 'body': body, 'rate': rate, 'product_id': product_id};
    self.call('POST', 'reviews', data, callback);
  };

  this.rate = function(product_ids, limit, page, callback) {
    if (typeof callback == 'undefined' && arguments.length) {
      var lastArgument = arguments[arguments.length - 1];
      callback = (typeof lastArgument == 'function') ? lastArgument : callback;
    }
    if (typeof product_ids == 'undefined' || typeof product_ids == 'function' || !product_ids) {
      product_ids = null;
    }
    if (typeof limit !== 'number' || !limit) {
      limit = 250;
    }
    if (typeof page !== 'number' || !page) {
      page = 1;
    }

    var data = {'limit': limit, 'page': page};
    if (product_ids !== null) {
      data.product_ids = product_ids;
    }

    self.call('GET', 'reviews/products', data, callback);
  };
};
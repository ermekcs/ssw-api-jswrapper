# SocialShopWave/Client

Simple [SocialShopWave API](http://socialshopwave.api-docs.io/) client in JavaScript


## Requirements

* jQuery 1.8+ (https://jquery.com).

How To Use
----------

#### Include the file ####

```html
<script type="text/javascript" src="ssw-client.js"></script>
```

#### Get access token ####

```javascript
sswclient = new SSWClient("YOUR_SSW_API_KEY", "YOUR_SSW_API_SECRET");
sswclient.getAppToken(callbackFunction);
```

GET Request Example
-------------------

Get user's wishlist

```javascript
userId = 1;
sswclient.getWishlist(userId, function(response) {
  userWishlist = response.data;
});
```